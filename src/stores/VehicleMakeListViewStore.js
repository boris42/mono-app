import { observable, computed, action } from 'mobx';

class VehicleMakeListViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  sort = {
    order: 'ascending',
    selection: 'id'
  };

  @observable
  pages = {
    currentPage: 1,
    makesPerPage: 4
  };

  @action
  onSortOrder = event => {
    this.sort.order = event.target.value;
  };

  @action
  onSortSelect = value => {
    this.sort.selection = value;
  };

  @action
  onChangePage = value => {
    this.pages.currentPage = value;
  };

  @computed
  get sortedVehicles() {
    return this.rootStore.vehicleMakeStore.vehicleMakes.slice().sort((a, b) => {
      if (this.sort.order === 'ascending') {
        if (this.sort.selection === 'name') {
          return a.name.localeCompare(b.name);
        }
        return a[this.sort.selection] - b[this.rootStore.selection];
      } else if (this.sort.order === 'descending') {
        if (this.sort.selection === 'name') {
          return b.name.localeCompare(a.name);
        }
        return b[this.sort.selection] - a[this.sort.selection];
      } else {
        return null;
      }
    });
  }

  @computed
  get indexOfLastMake() {
    return this.pages.currentPage * this.pages.makesPerPage;
  }

  @computed
  get indexOfFirstMake() {
    return this.indexOfLastMake - this.pages.makesPerPage;
  }

  @computed
  get currentMakes() {
    return this.sortedVehicles.slice(
      this.indexOfFirstMake,
      this.indexOfLastMake
    );
  }
}

export default VehicleMakeListViewStore;
