import { observable, action } from 'mobx';

class VehicleModelStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }
  @observable
  vehicleModels = [
    {
      makeId: 100,
      id: 101,
      name: 'Civic',
      year: 2000,
      price: 5000,
      imgUrl: 'https://m.media-amazon.com/images/I/71xeM5KQVFL._UY560_.jpg'
    },
    {
      makeId: 100,
      id: 102,
      name: 'CR-V',
      year: 2010,
      price: 10000,
      imgUrl:
        'https://file.kbb.com/kbb/vehicleimage/housenew/480x360/2010/2010-honda-cr-v-frontside_htcrv101.jpg'
    },
    {
      makeId: 200,
      id: 201,
      name: 'Camry',
      year: 2013,
      price: 8000,
      imgUrl:
        'https://static.cargurus.com/images/site/2013/08/02/16/54/2013_toyota_camry-pic-8737316483710024806-640x480.jpeg'
    },
    {
      makeId: 200,
      id: 202,
      name: 'RAV4',
      year: 2015,
      price: 15000,
      imgUrl:
        'https://i.gaw.to/content/photos/18/58/185818_2015_Toyota_RAV4.jpg'
    },
    {
      makeId: 200,
      id: 203,
      name: 'Highlander',
      year: 2014,
      price: 17000,
      imgUrl:
        'https://static.cargurus.com/images/site/2014/01/13/18/09/2014_toyota_highlander_hybrid-pic-985906573636546719-1600x1200.jpeg'
    },
    {
      makeId: 200,
      id: 204,
      name: 'Tacoma',
      year: 2012,
      price: 18000,
      imgUrl:
        'https://www.formfithoodprotectors.com/wp-content/uploads/2016/11/Toyota-Tacoma-2012-2015-FormFit-Hood-Protector-HD-20E12-c.jpg'
    },
    {
      makeId: 200,
      id: 205,
      name: 'Corolla',
      year: 2018,
      price: 12000,
      imgUrl: 'https://i.ytimg.com/vi/AQilXCKlyvc/maxresdefault.jpg'
    },
    {
      makeId: 300,
      id: 301,
      name: 'Rogue',
      year: 2017,
      price: 16000,
      imgUrl:
        'https://assets.nydailynews.com/polopoly_fs/1.2904700.1481297468!/img/httpImage/image.jpg_gen/derivatives/article_750/nydn-2017-nissan-rogue-hybrid-sl-dark-gray-front-quarter.jpg'
    },
    {
      makeId: 300,
      id: 302,
      name: 'Sentra',
      year: 2015,
      price: 13500,
      imgUrl:
        'https://invimg.autofunds.com/InventoryImages/2018/09/01/2644_1316860_3658623_7433301432018_90.jpg'
    },
    {
      makeId: 400,
      id: 401,
      name: 'F-series',
      year: 2013,
      price: 10000,
      imgUrl:
        'https://cdn-ds.com/stock/2013-Ford-F-150-Lariat-Utica-NY/seo/VAMP7838-1FTFX1ET3DFC68509/sz_155144/1FTFX1ET3DFC68509_5267156b2485beac.jpg'
    },
    {
      makeId: 400,
      id: 402,
      name: 'Explorer',
      year: 2014,
      price: 11000,
      imgUrl:
        'https://i.gaw.to/vehicles/photos/05/05/050583_2014_ford_Explorer.jpg?1024x640'
    },
    {
      makeId: 400,
      id: 403,
      name: 'Escape',
      year: 2015,
      price: 11500,
      imgUrl:
        'http://3.bp.blogspot.com/-VV83Skwc1Y4/VE9QuV5o7RI/AAAAAAAAUyU/LB5aIZYHUDE/s1600/2015_ford_escape.jpg'
    },
    {
      makeId: 500,
      id: 501,
      name: 'Silverado',
      year: 2009,
      price: 8000,
      imgUrl:
        'https://39838bd83cbb26a9c5d5-91a04adb5fa5f57dd3ca4ff7e41af49e.ssl.cf1.rackcdn.com/3GCEC23059G241467/f65392ddc73ff5db6a09bb75621729f8.jpg'
    },
    {
      makeId: 500,
      id: 502,
      name: 'Equinox',
      year: 2011,
      price: 9000,
      imgUrl:
        'https://images.hgmsites.net/hug/2011-chevrolet-equinox_100333572_h.jpg'
    },
    {
      makeId: 600,
      id: 601,
      name: 'Grand Cherokee',
      year: 2016,
      price: 20000,
      imgUrl:
        'https://static.carsdn.co/cldstatic/wp-content/uploads/img1672599400-1441032822050.jpg'
    },
    {
      makeId: 600,
      id: 602,
      name: 'Cherokee',
      year: 2016,
      price: 16000,
      imgUrl:
        'https://res.cloudinary.com/carsguide/image/upload/f_auto,fl_lossy,q_auto,t_cg_hero_large/v1/editorial/Jeep-Cherokee-75th-Anniversary-black-2016-%2811%29.jpg'
    },
    {
      makeId: 600,
      id: 603,
      name: 'Wrangler',
      year: 2017,
      price: 16000,
      imgUrl: 'https://i.ytimg.com/vi/kpWfZI5vS7A/maxresdefault.jpg'
    }
  ];

  // Here or in VehicleModelEditViewStore??
  @action
  onEditModel = e => {
    this.rootStore.vehicleModelEditViewStore.selectedModel[e.target.name] =
      e.target.value;
  };

  @action
  onCreateModel = newModel => {
    this.vehicleModels.push(newModel);
    Object.keys(this.rootStore.vehicleModelCreateViewStore.model).map(item => {
      return (this.rootStore.vehicleModelCreateViewStore.model[item] = null);
    });
  };
}

export default VehicleModelStore;
