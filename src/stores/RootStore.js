import VehicleMakeStore from './VehicleMakeStore';
import VehicleModelStore from './VehicleModelStore';
import VehicleMakeListViewStore from './VehicleMakeListViewStore';
import VehicleMakeEditViewStore from './VehicleMakeEditViewStore';
import VehicleMakeCreateViewStore from './VehicleMakeCreateViewStore';
import VehicleModelListViewStore from './VehicleModelListViewStore';
import VehicleModelEditViewStore from './VehicleModelEditViewStore';
import VehicleModelCreateViewStore from './VehicleModelCreateViewStore';

class RootStore {
  constructor() {
    this.vehicleMakeStore = new VehicleMakeStore(this);
    this.vehicleModelStore = new VehicleModelStore(this);
    this.vehicleMakeListViewStore = new VehicleMakeListViewStore(this);
    this.vehicleMakeEditViewStore = new VehicleMakeEditViewStore(this);
    this.vehicleMakeCreateViewStore = new VehicleMakeCreateViewStore(this);
    this.vehicleModelListViewStore = new VehicleModelListViewStore(this);
    this.vehicleModelEditViewStore = new VehicleModelEditViewStore(this);
    this.vehicleModelCreateViewStore = new VehicleModelCreateViewStore(this);
  }
}

const rootStore = new RootStore();
export default rootStore;
