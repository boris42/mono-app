import { observable, action, computed } from 'mobx';

class VehicleMakeCreateViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  name = '';

  @action
  onInputName = e => {
    this.name = e.target.value;
  };

  @computed
  get newMake() {
    return {
      id:
        this.rootStore.vehicleMakeStore.vehicleMakes[
          this.rootStore.vehicleMakeStore.vehicleMakes.length - 1
        ].id + 100,
      name: this.name
    };
  }
}

export default VehicleMakeCreateViewStore;
