import { observable, action, computed } from 'mobx';

class VehicleModelEditViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  selected;

  @action
  onModelSelect(id) {
    this.selected = id;
  }

  @computed
  get selectedModel() {
    return this.rootStore.vehicleModelStore.vehicleModels.find(model => {
      return model.id === this.selected;
    });
  }

  @computed
  get selectedMake() {
    return this.rootStore.vehicleMakeStore.vehicleMakes.find(make => {
      return make.id === this.selectedModel.makeId;
    });
  }
}

export default VehicleModelEditViewStore;
