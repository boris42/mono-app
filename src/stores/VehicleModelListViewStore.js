import { observable, computed, action } from 'mobx';

class VehicleModelListViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  sort = {
    order: 'ascending',
    selection: 'id'
  };

  @observable
  pages = {
    currentPage: 1,
    modelsPerPage: 4
  };

  @observable
  filter = {
    minPrice: 0,
    maxPrice: 50000,
    minYear: 1950,
    maxYear: new Date().getFullYear()
  };

  @action
  onSortOrder = event => {
    this.sort.order = event.target.value;
    this.pages.currentPage = 1;
  };

  @action
  onSortSelect = value => {
    this.sort.selection = value;
    this.pages.currentPage = 1;
  };

  @action
  onChangePage = value => {
    this.pages.currentPage = value;
  };

  @action
  onUpdatePrice = value => {
    this.filter.minPrice = value[0];
    this.filter.maxPrice = value[1];
    this.pages.currentPage = 1;
  };

  @action
  onUpdateYear = value => {
    this.filter.minYear = value[0];
    this.filter.maxYear = value[1];
    this.pages.currentPage = 1;
  };

  @computed
  get sortedVehicles() {
    return this.filteredVehicles.slice().sort((a, b) => {
      const aMake = this.rootStore.vehicleMakeStore.vehicleMakes.find(make => {
        return make.id === a.makeId;
      });
      const bMake = this.rootStore.vehicleMakeStore.vehicleMakes.find(make => {
        return make.id === b.makeId;
      });

      if (this.sort.order === 'ascending') {
        if (this.sort.selection === 'name') {
          return (aMake.name + a.name).localeCompare(bMake.name + b.name);
        }
        return a[this.sort.selection] - b[this.sort.selection];
      } else if (this.sort.order === 'descending') {
        if (this.sort.selection === 'name') {
          return (bMake.name + b.name).localeCompare(aMake.name + a.name);
        }
        return b[this.sort.selection] - a[this.sort.selection];
      } else {
        return null;
      }
    });
  }

  @computed
  get makeFilters() {
    return this.rootStore.vehicleMakeStore.vehicleMakes.filter(make => {
      return make.filter;
    });
  }

  @computed
  get filteredVehicles() {
    if (this.makeFilters.length > 0) {
      return this.rootStore.vehicleModelStore.vehicleModels.filter(model => {
        return (
          this.makeFilters.some(item => {
            return item.id === model.makeId;
          }) &&
          (model.price >= this.filter.minPrice &&
            model.price <= this.filter.maxPrice) &&
          (model.year >= this.filter.minYear &&
            model.year <= this.filter.maxYear)
        );
      });
    }
    return this.rootStore.vehicleModelStore.vehicleModels.filter(model => {
      return (
        model.price >= this.filter.minPrice &&
        model.price <= this.filter.maxPrice &&
        (model.year >= this.filter.minYear && model.year <= this.filter.maxYear)
      );
    });
  }

  @computed
  get indexOfLastModel() {
    return this.pages.currentPage * this.pages.modelsPerPage;
  }

  @computed
  get indexOfFirstModel() {
    return this.indexOfLastModel - this.pages.modelsPerPage;
  }

  @computed
  get currentModels() {
    return this.sortedVehicles.slice(
      this.indexOfFirstModel,
      this.indexOfLastModel
    );
  }
}

export default VehicleModelListViewStore;
