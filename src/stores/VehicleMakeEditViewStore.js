import { observable, action, computed } from 'mobx';

class VehicleMakeEditViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  selected;

  @action
  onMakeSelect(id) {
    this.selected = id;
  }

  @computed
  get selectedMake() {
    return this.rootStore.vehicleMakeStore.vehicleMakes.find(make => {
      return make.id === this.selected;
    });
  }
}

export default VehicleMakeEditViewStore;
