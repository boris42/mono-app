import { observable, action } from 'mobx';

class VehicleMakeStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  vehicleMakes = [
    {
      id: 100,
      name: 'Honda',
      filter: false
    },
    {
      id: 200,
      name: 'Toyota',
      filter: false
    },
    {
      id: 300,
      name: 'Nissan',
      filter: false
    },
    {
      id: 400,
      name: 'Ford',
      filter: false
    },
    {
      id: 500,
      name: 'Chevrolet',
      filter: false
    },
    {
      id: 600,
      name: 'Jeep',
      filter: false
    }
  ];

  // Here or in VehicleMakeEditViewStore??
  @action
  onEditMake = e => {
    this.rootStore.vehicleMakeEditViewStore.selectedMake[e.target.name] =
      e.target.value;
  };

  @action
  onCreateMake = newMake => {
    this.vehicleMakes.push(newMake);
    this.rootStore.vehicleMakeCreateViewStore.name = '';
  };

  @action
  onCheckboxFilter = make => {
    make.filter = !make.filter;
    this.rootStore.vehicleModelListViewStore.pages.currentPage = 1;
  };
}

export default VehicleMakeStore;
