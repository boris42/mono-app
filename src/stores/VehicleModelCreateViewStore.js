import { observable, action, computed } from 'mobx';

class VehicleModelCreateViewStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @observable
  model = {
    makeId: null,
    id: null,
    name: null,
    year: null,
    price: null,
    imgUrl: null
  };

  @action
  onSelectMake = value => {
    this.model.makeId = value;
    this.model.id = this.newId;
  };

  @action
  onInput = e => {
    this.model[e.target.name] = e.target.value;
  };

  @computed
  get currentModels() {
    return this.rootStore.vehicleModelStore.vehicleModels.filter(model => {
      return model.makeId === this.model.makeId;
    });
  }

  @computed
  get newId() {
    if (this.currentModels.length > 0) {
      return this.currentModels[this.currentModels.length - 1].id + 1;
    }
    return this.model.makeId + 1;
  }

  @computed
  get newModel() {
    return {
      makeId: this.model.makeId,
      id: this.newId,
      name: this.model.name,
      year: this.model.year,
      price: this.model.price,
      imgUrl: this.model.imgUrl
    };
  }
}

export default VehicleModelCreateViewStore;
