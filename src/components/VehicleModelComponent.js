import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { Col, Card } from 'antd';

@inject(stores => ({
  vehicleMakeStore: stores.rootStore.vehicleMakeStore
}))
class VehicleModelComponent extends Component {
  render() {
    const model = this.props.model;
    const make = this.props.vehicleMakeStore.vehicleMakes.find(make => {
      return make.id === model.makeId;
    });

    return (
      <Col xs={24} sm={12} md={12} lg={6}>
        <div className="vehicle-wrapper">
          <Card
            cover={<img alt={`${model.name}`} src={model.imgUrl} />}
            hoverable
          >
            <h2>{make.name}</h2>
            <h4>{model.name}</h4>
            <p>Year: {model.year}</p>
            <p>Price: ${model.price}</p>
          </Card>
        </div>
      </Col>
    );
  }
}

export default VehicleModelComponent;
