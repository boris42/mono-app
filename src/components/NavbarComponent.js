import React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu } from 'antd';

function Navbar() {
  return (
    <Menu theme="light" mode="horizontal" style={{ lineHeight: '64px' }}>
      <Menu.Item>
        <NavLink to="/">Home</NavLink>
      </Menu.Item>
      <Menu.Item>
        <NavLink to="/makes">Makes</NavLink>
      </Menu.Item>
      <Menu.Item>
        <NavLink to="/models">Models</NavLink>
      </Menu.Item>
    </Menu>
  );
}

export default Navbar;
