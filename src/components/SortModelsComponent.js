import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Radio, Select } from 'antd';

const { Option } = Select;

@inject(stores => ({
  vehicleModelListViewStore: stores.rootStore.vehicleModelListViewStore
}))
@observer
class SortModelsComponent extends Component {
  render() {
    const { vehicleModelListViewStore } = this.props;

    return (
      <div className="sort-wrapper">
        <div className="sort-radio">
          <Radio.Group
            onChange={vehicleModelListViewStore.onSortOrder}
            value={vehicleModelListViewStore.sort.order}
          >
            <Radio value={'ascending'}>Ascending</Radio>
            <Radio value={'descending'}>Descending</Radio>
          </Radio.Group>
        </div>
        <div>
          <Select
            defaultValue={vehicleModelListViewStore.sort.selection}
            style={{ width: 120 }}
            onChange={vehicleModelListViewStore.onSortSelect}
          >
            <Option value="id">ID</Option>
            <Option value="name">Name</Option>
            <Option value="price">Price</Option>
            <Option value="year">Year</Option>
          </Select>
        </div>
      </div>
    );
  }
}

export default SortModelsComponent;
