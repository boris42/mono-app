import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Checkbox, Slider } from 'antd';

@inject(stores => ({
  vehicleMakeStore: stores.rootStore.vehicleMakeStore,
  vehicleModelListViewStore: stores.rootStore.vehicleModelListViewStore
}))
@observer
class FilterModelsComponent extends Component {
  render() {
    const { vehicleMakeStore } = this.props;
    const { vehicleModelListViewStore } = this.props;

    return (
      <div>
        <h1>Filter</h1>
        <form>
          <div className="make-filter">
            {vehicleMakeStore.vehicleMakes.map(make => {
              return (
                <div key={make.id} className="checkbox-item">
                  <Checkbox
                    onChange={() =>
                      vehicleMakeStore.onCheckboxFilter(make)
                    }
                    name={make.name}
                    checked={make.filter}
                  >
                    {make.name}
                  </Checkbox>
                </div>
              );
            })}
          </div>
          <div className="price-filter">
            <div>
              <h3>Price</h3>
              <Slider
                range
                min={0}
                max={50000}
                defaultValue={[
                  vehicleModelListViewStore.filter.minPrice,
                  vehicleModelListViewStore.filter.maxPrice
                ]}
                onChange={vehicleModelListViewStore.onUpdatePrice}
              />
            </div>
          </div>
          <div className="year-filter">
            <h3>Year</h3>
            <Slider
              range
              min={1950}
              max={new Date().getFullYear()}
              defaultValue={[
                vehicleModelListViewStore.filter.minYear,
                vehicleModelListViewStore.filter.maxYear
              ]}
              onChange={vehicleModelListViewStore.onUpdateYear}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default FilterModelsComponent;
