import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Pagination } from 'antd';

@observer
class PaginationComponent extends Component {
  render() {
    const { store } = this.props;

    return (
      <div>
        {store.sortedVehicles.length ? (
          <div className="pagination">
            <Pagination
              current={store.pages.currentPage}
              onChange={store.onChangePage}
              total={store.sortedVehicles.length}
              pageSize={4}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default PaginationComponent;
