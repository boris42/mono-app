import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Radio, Select } from 'antd';

const { Option } = Select;

@inject(stores => ({
  vehicleMakeListViewStore: stores.rootStore.vehicleMakeListViewStore
}))
@observer
class SortMakesComponent extends Component {
  render() {
    const { vehicleMakeListViewStore } = this.props;

    return (
      <div className="sort-wrapper">
        <div className="sort-radio">
          <Radio.Group
            onChange={vehicleMakeListViewStore.onSortOrder}
            value={vehicleMakeListViewStore.sort.order}
          >
            <Radio value={'ascending'}>Ascending</Radio>
            <Radio value={'descending'}>Descending</Radio>
          </Radio.Group>
        </div>
        <div>
          <Select
            defaultValue={vehicleMakeListViewStore.sort.selection}
            style={{ width: 120 }}
            onChange={vehicleMakeListViewStore.onSortSelect}
          >
            <Option value="id">ID</Option>
            <Option value="name">Name</Option>
          </Select>
        </div>
      </div>
    );
  }
}

export default SortMakesComponent;
