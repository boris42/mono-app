import React from 'react';
import { Col, Card } from 'antd';

function VehicleMakeComponent(props) {
  return (
    <Col xs={24} sm={12} md={12} lg={6}>
      <div className="vehicle-wrapper">
        <Card hoverable>{props.make.name}</Card>
      </div>
    </Col>
  );
}

export default VehicleMakeComponent;
