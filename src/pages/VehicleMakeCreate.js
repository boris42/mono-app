import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Input, Button } from 'antd';

@inject(stores => ({
  vehicleMakeCreateViewStore: stores.rootStore.vehicleMakeCreateViewStore,
  vehicleMakeStore: stores.rootStore.vehicleMakeStore
}))
@observer
class VehicleMakeCreate extends Component {
  render() {
    const { vehicleMakeCreateViewStore } = this.props;
    const { vehicleMakeStore } = this.props;

    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={12} lg={12}>
          <form>
            <div className="input-wrapper">
              <Input
                name="name"
                value={vehicleMakeCreateViewStore.name}
                onChange={vehicleMakeCreateViewStore.onInputName}
                addonAfter="Name"
              />
            </div>
            <Button
              type="primary"
              onClick={() =>
                vehicleMakeStore.onCreateMake(
                  vehicleMakeCreateViewStore.newMake
                )
              }
              disabled={vehicleMakeCreateViewStore.name === ''}
            >
              Create
            </Button>
          </form>
          <div className="vehicle-details-wrapper">
            <h1>ID: {vehicleMakeCreateViewStore.newMake.id}</h1>
            <h1>Name: {vehicleMakeCreateViewStore.name}</h1>
          </div>
        </Col>
      </Row>
    );
  }
}

export default VehicleMakeCreate;
