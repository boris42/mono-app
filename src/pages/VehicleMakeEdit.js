import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Input } from 'antd';

@inject(stores => ({
  vehicleMakeEditViewStore: stores.rootStore.vehicleMakeEditViewStore,
  vehicleMakeStore: stores.rootStore.vehicleMakeStore
}))
@observer
class VehicleMakeEdit extends Component {
  render() {
    const { vehicleMakeEditViewStore } = this.props;
    const { vehicleMakeStore } = this.props;

    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={12} lg={12}>
          <form>
            <div className="input-wrapper">
              <Input
                name="name"
                value={vehicleMakeEditViewStore.selectedMake.name}
                onChange={vehicleMakeStore.onEditMake}
                addonAfter="Name"
              />
            </div>
          </form>
          <div className="vehicle-details-wrapper">
            <h1>{vehicleMakeEditViewStore.selectedMake.name}</h1>
          </div>
        </Col>
      </Row>
    );
  }
}

export default VehicleMakeEdit;
