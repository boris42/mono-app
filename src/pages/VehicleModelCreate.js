import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Input, Button, Select } from 'antd';

const { Option } = Select;

@inject(stores => ({
  vehicleModelStore: stores.rootStore.vehicleModelStore,
  vehicleModelCreateViewStore: stores.rootStore.vehicleModelCreateViewStore,
  vehicleMakeStore: stores.rootStore.vehicleMakeStore,
  vehicleModelListViewStore: stores.rootStore.vehicleModelListViewStore
}))
@observer
class VehicleModelCreate extends Component {
  render() {
    const {
      vehicleModelStore,
      vehicleModelCreateViewStore,
      vehicleMakeStore,
      vehicleModelListViewStore
    } = this.props;
    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={12} lg={12}>
          <form>
            <div className="input-wrapper">
              <Select
                defaultValue="Please select vehicle make"
                onChange={vehicleModelCreateViewStore.onSelectMake}
              >
                {vehicleMakeStore.vehicleMakes.map(make => {
                  return (
                    <Option key={make.id} value={make.id}>
                      {make.name}
                    </Option>
                  );
                })}
              </Select>
            </div>
            <div className="input-wrapper">
              <Input
                name="name"
                value={vehicleModelCreateViewStore.model.name}
                onChange={vehicleModelCreateViewStore.onInput}
                addonAfter="Name"
              />
            </div>
            <div className="input-wrapper">
              <Input
                type="number"
                name="price"
                value={vehicleModelCreateViewStore.model.price}
                onChange={vehicleModelCreateViewStore.onInput}
                addonAfter="Price"
                min={vehicleModelListViewStore.filter.minPrice}
                max={vehicleModelListViewStore.filter.maxPrice}
              />
            </div>
            <div className="input-wrapper">
              <Input
                type="number"
                name="year"
                value={vehicleModelCreateViewStore.model.year}
                onChange={vehicleModelCreateViewStore.onInput}
                addonAfter="Year"
                min={vehicleModelListViewStore.filter.minYear}
                max={vehicleModelListViewStore.filter.maxYear}
              />
            </div>
            <div className="input-wrapper">
              <Input
                name="imgUrl"
                value={vehicleModelCreateViewStore.model.imgUrl}
                onChange={vehicleModelCreateViewStore.onInput}
                addonAfter="Img Url"
              />
            </div>
            <Button
              type="primary"
              onClick={() =>
                vehicleModelStore.onCreateModel(
                  vehicleModelCreateViewStore.newModel
                )
              }
              disabled={Object.values(vehicleModelCreateViewStore.model).some(
                x => x === null || x === ''
              )}
            >
              Create
            </Button>
          </form>
          <div className="vehicle-details-wrapper">
            <h1>ID: {vehicleModelCreateViewStore.model.id}</h1>
            <h1>Name: {vehicleModelCreateViewStore.model.name}</h1>
            <h1>Price: {vehicleModelCreateViewStore.model.price}</h1>
            <h1>Year: {vehicleModelCreateViewStore.model.year}</h1>
          </div>
        </Col>
        {vehicleModelCreateViewStore.model.imgUrl ? (
          <Col xs={24} sm={24} md={12} lg={12}>
            <div className="vehicle-image-wrapper">
              <img
                src={vehicleModelCreateViewStore.model.imgUrl}
                alt={`${vehicleModelCreateViewStore.model.name}`}
              />
            </div>
          </Col>
        ) : null}
      </Row>
    );
  }
}

export default VehicleModelCreate;
