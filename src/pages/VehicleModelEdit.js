import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Input } from 'antd';

@inject(stores => ({
  vehicleModelEditViewStore: stores.rootStore.vehicleModelEditViewStore,
  vehicleModelStore: stores.rootStore.vehicleModelStore
}))
@observer
class VehicleModelEdit extends Component {
  render() {
    const { vehicleModelEditViewStore, vehicleModelStore } = this.props;

    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={12} lg={12}>
          <form>
            <div className="input-wrapper">
              <Input
                name="name"
                value={vehicleModelEditViewStore.selectedModel.name}
                onChange={vehicleModelStore.onEditModel}
                addonAfter="Model"
              />
            </div>
            <div className="input-wrapper">
              <Input
                type="number"
                name="price"
                value={vehicleModelEditViewStore.selectedModel.price}
                onChange={vehicleModelStore.onEditModel}
                addonAfter="Price"
              />
            </div>
            <div className="input-wrapper">
              <Input
                type="number"
                name="year"
                value={vehicleModelEditViewStore.selectedModel.year}
                onChange={vehicleModelStore.onEditModel}
                addonAfter="Year"
              />
            </div>
            <div className="input-wrapper">
              <Input
                name="imgUrl"
                value={vehicleModelEditViewStore.selectedModel.imgUrl}
                onChange={vehicleModelStore.onEditModel}
                addonAfter="Img URL"
              />
            </div>
          </form>
          <div className="vehicle-details-wrapper">
            <h1>{vehicleModelEditViewStore.selectedMake.name}</h1>
            <h3>{vehicleModelEditViewStore.selectedModel.name}</h3>
            <p>Year: {vehicleModelEditViewStore.selectedModel.year}</p>
            <p>Price: ${vehicleModelEditViewStore.selectedModel.price}</p>
          </div>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12}>
          <div className="vehicle-image-wrapper">
            <img
              src={vehicleModelEditViewStore.selectedModel.imgUrl}
              alt={`${vehicleModelEditViewStore.selectedModel.name}`}
            />
          </div>
        </Col>
      </Row>
    );
  }
}

export default VehicleModelEdit;
