import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'antd';

import SortModelsComponent from '../components/SortModelsComponent';
import PaginationComponent from '../components/PaginationComponent';
import FilterModelsComponent from '../components/FilterModelsComponent';
import VehicleModelComponent from '../components/VehicleModelComponent';

@inject(stores => ({
  vehicleModelListViewStore: stores.rootStore.vehicleModelListViewStore,
  vehicleModelEditViewStore: stores.rootStore.vehicleModelEditViewStore
}))
@observer
class VehicleModelList extends Component {
  render() {
    const { vehicleModelListViewStore } = this.props;
    const { vehicleModelEditViewStore } = this.props;

    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={6} lg={6}>
          <SortModelsComponent />
          <FilterModelsComponent />
        </Col>
        <Col xs={24} sm={24} md={18} lg={18}>
          <Row gutter={16}>
            {vehicleModelListViewStore.currentModels.map(model => {
              return (
                <Link
                  to={`/models/edit/${model.id}`}
                  key={model.id}
                  onClick={() =>
                    vehicleModelEditViewStore.onModelSelect(model.id)
                  }
                >
                  <VehicleModelComponent model={model} />
                </Link>
              );
            })}
          </Row>
          <PaginationComponent store={vehicleModelListViewStore} />
          <Link to="/models/create">
            <Button type="primary">New</Button>
          </Link>
        </Col>
      </Row>
    );
  }
}

export default VehicleModelList;
