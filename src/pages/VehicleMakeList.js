import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'antd';

import SortMakesComponent from '../components/SortMakesComponent';
import PaginationComponent from '../components/PaginationComponent';
import VehicleMakeComponent from '../components/VehicleMakeComponent';

@inject(stores => ({
  vehicleMakeListViewStore: stores.rootStore.vehicleMakeListViewStore,
  vehicleMakeEditViewStore: stores.rootStore.vehicleMakeEditViewStore
}))
@observer
class VehicleMakeList extends Component {
  render() {
    const { vehicleMakeListViewStore } = this.props;
    const { vehicleMakeEditViewStore } = this.props;

    return (
      <Row gutter={24}>
        <Col xs={24} sm={24} md={6} lg={6}>
          <SortMakesComponent />
          {/* <FilterComponent /> */}
        </Col>
        <Col xs={24} sm={24} md={18} lg={18}>
          <Row gutter={16}>
            {vehicleMakeListViewStore.currentMakes.map(make => {
              return (
                <Link
                  to={`/makes/edit/${make.id}`}
                  key={make.id}
                  onClick={() => vehicleMakeEditViewStore.onMakeSelect(make.id)}
                >
                  <VehicleMakeComponent make={make} />
                </Link>
              );
            })}
          </Row>
          <PaginationComponent store={vehicleMakeListViewStore} />
          <Link to="/makes/create">
            <Button type="primary">New</Button>
          </Link>
        </Col>
      </Row>
    );
  }
}

export default VehicleMakeList;
