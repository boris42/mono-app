import React from 'react';
import './App.css';
import { Route } from 'react-router-dom';
import { Layout, Row, Col } from 'antd';

import NavbarComponent from './components/NavbarComponent';
import Home from './pages/Home';
import VehicleMakeList from './pages/VehicleMakeList';
import VehicleMakeEdit from './pages/VehicleMakeEdit';
import VehicleMakeCreate from './pages/VehicleMakeCreate';
import VehicleModelList from './pages/VehicleModelList';
import VehicleModelEdit from './pages/VehicleModelEdit';
import VehicleModelCreate from './pages/VehicleModelCreate';

const { Header, Content } = Layout;

function App() {
  return (
    <Layout className="layout">
      <Header className="main-header">
        <NavbarComponent />
      </Header>
      <Content className="container">
        <Row type="flex" justify="center">
          <Col xs={18} sm={18} md={22} lg={22}>
            <Route exact path="/" component={Home} />
            <Route exact path="/makes" component={VehicleMakeList} />
            <Route path="/makes/edit/:id" component={VehicleMakeEdit} />
            <Route path="/makes/create" component={VehicleMakeCreate} />
            <Route exact path="/models" component={VehicleModelList} />
            <Route path="/models/edit/:id" component={VehicleModelEdit} />
            <Route path="/models/create" component={VehicleModelCreate} />
          </Col>
        </Row>
      </Content>
    </Layout>
  );
}

export default App;
